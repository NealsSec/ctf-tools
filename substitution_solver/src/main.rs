mod usage;

use std::{env, process, collections::HashMap};

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        usage::print_usage();
        panic!("ERROR: No input files");
    }
    
    let first_arg = &args[1];

    if first_arg == "-h" || first_arg == "--help" {
        usage::print_usage();
        process::exit(0);
    }

    let contents = std::fs::read_to_string(first_arg).
        expect("ERROR: File not found");

    let mut letter_map: HashMap<char, u8> = HashMap::new();
    for letter in contents.chars() {
        let entry = letter_map.entry(letter).or_insert(0);
        *entry += 1;
    }
    for entry in letter_map {
        println!("{:?}", entry);
    }
}

/*
Improvements:
- remove white space and punctuation
- Make all chars upper / lower
 */