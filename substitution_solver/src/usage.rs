pub fn print_usage() {
    println!("Usage: substitution_solver [FILE]");
    println!("substitution_solver tallys each character in the passed file and lists the information gathered.\n");
    println!("  -h, --help      print usage information\n");
}